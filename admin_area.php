<?php
include'database.php';
session_start();
$name = $_SESSION['adminname'];
$dbid = $_SESSION['adminid'];

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $name ?></title>
        <link rel="stylesheet" href="css/css_admin.css">
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/subscribe_function.js"></script> 
        <script>
            $(document).ready(function () {
                $("select").change(function () {
                   var adminid = this.value;
                   var hrefValue = $('.assignee').attr('href');
                   var redirectUrl = hrefValue+'&assignee='+adminid;
                   $('.assignee').attr('href',redirectUrl);
                   // window.location.href = 'admin_id.php?admin_id=' + adminid;
//                    $.ajax({
//                        type: 'POST',
//                        url: 'assign_issue_process.php',
//                        data: {'variable': adminid},
//                    });
                });
            });
        </script>
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="index.php"><img src="images/oxlogo.png"></a> 
            </div>
            <div class="top-left">
                <a href="logout.php">Logout</a> 
            </div>
            <div class="top-left">
                <?php
                if (!$name) {
                    header('location:index.php');
                } else {
                    echo "<h2>Welcome," . $name . "!</h2>";
                }
                ?>
            </div>
        </header>
        <section>
            <div id='assigned_issue'>
                <h2>Assigned Issues</h2>
                <div class="all_issue_name"><h3>ID</h3></div>
                <div class="all_issue_name"><h3>Title</h3></div>
                <div class="all_issue_name"><h3>Created at</h3></div>
                <div class="all_issue_name"><h3>Updated at</h3></div>
                <div class="all_issue_name"><h3>Status</h3></div>
                <div class="all_issue_name"><h3>Priority</h3></div>
                <div class="all_issue_name"><h3>Description</h3></div>
                <div class="all_issue_name"><h3>Re-assign to</h3></div>
                <div class="all_issue_name"><h3>Action</h3></div>
                <?php
                $data = new User();
                $result = $data->admin_view_issue($dbid);
                foreach ($result as $get) {
                    $issueid = $get['Issue_id'];
                    ?>

                    <div class="all_issue_text"><?php echo $get['Issue_id']; ?></div>
                    <div class="all_issue_text"><?php echo $get['Issue_title']; ?></div>
                    <div class="all_issue_text"><?php echo $get['Issue_created_at']; ?></div>
                    <div class="all_issue_text"><?php echo $get['Issue_updated_at']; ?></div>
                    <div class="all_issue_text"><?php
                $status = $get['Issue_status'];
                if ($status == 1) {
                    echo 'Open';
                }
                if ($status == 2) {
                    echo "Inprogress";
                }
                if ($status == 3) {
                    echo "Closed";
                }
                    ?></div>
                    <div class="all_issue_text"><?php
                    $priority = $get['Issue_priority'];
                    if ($priority == 1) {
                        echo 'Low';
                    }
                    if ($priority == 2) {
                        echo "Medium";
                    }
                    if ($priority == 3) {
                        echo "High";
                    }
                    ?></div>
                    <div class="all_issue_des"><?php echo $get['Issue_description']; ?></div>
                    <div class='all_issue_text'>  <select name='admin_name' required><option value=''>Select</option>
                            <?php
                            $adminname = $data->select_admin_name();
                            foreach ($adminname as $admin) {
                                ?>

                                <option value="<?php echo $admin['User_id']; ?>" > <?php echo $admin['User_name']; ?></option>


                            <?php } ?></select> 

                        <?php
                       
                       
                        echo "<a class='assignee' href='assign_issue_process.php?rowid=$issueid'>Assign</a>"
                        ?> </div>
                      <div class="issue_text"><?php echo "<a href='issue_close_process.php?rowid=$issueid'>Close</a>" ?></div>
                    <?php } ?>


            </div>

            <?php
            if (isset($_SESSION['closemsg'])) {
                echo $_SESSION['closemsg'];
                unset($_SESSION['closemsg']);
            }
            if (isset($_SESSION['assignmsg'])) {
                echo $_SESSION['assignmsg'];
                unset($_SESSION['assignmsg']);
            }
            ?>


            <div id='closed_issue'>
                <h2>Closed Issues</h2>
                <div class="issue_name"><h3>ID</h3></div>
                <div class="issue_name"><h3>Title</h3></div>
                <div class="issue_name"><h3>Created at</h3></div>
                <div class="issue_name"><h3>Updated at</h3></div>
                <div class="issue_name"><h3>Status</h3></div>
                <div class="issue_name"><h3>Priority</h3></div>
                <div class="issue_name"><h3>Description</h3></div>

                <?php
                $result = $data->view_closed_issue($dbid);
                foreach ($result as $get) {
                    ?>
                    <div class="issue_text"><?php echo $get['Issue_id']; ?></div>
                    <div class="issue_text"><?php echo $get['Issue_title']; ?></div>
                    <div class="issue_text"><?php echo $get['Issue_created_at']; ?></div>
                    <div class="issue_text"><?php echo $get['Issue_updated_at']; ?></div>
                    <div class="issue_text"><?php
                $status = $get['Issue_status'];
                if ($status == 1) {
                    echo 'Open';
                }
                if ($status == 2) {
                    echo "Inprogress";
                }
                if ($status == 3) {
                    echo "Closed";
                }
                    ?></div>
                    <div class="issue_text"><?php
                    $priority = $get['Issue_priority'];
                    if ($priority == 1) {
                        echo 'Low';
                    }
                    if ($priority == 2) {
                        echo "Medium";
                    }
                    if ($priority == 3) {
                        echo "High";
                    }
                    ?></div>
                    <div class="issue_des"><?php echo $get['Issue_description']; ?></div>

                <?php } ?>
            </div>
        </section>
        <footer>
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <div id="socialmedia"> 
                    <a class='social' title="Find us on Facebook" href="https://www.facebook.com/oxsoftwares" target="_blank">Facebook</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Follow us on Twitter" href="https://twitter.com/oxsoftwares" target="_blank">Twitter</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Connect with us on Google+"  href="https://plus.google.com/+oxsoftwares" target="_blank">Google+</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Find us on LinkedIn" href="https://www.linkedin.com/company/oxsoftwares" tooltip=''target="_blank">LinkedIn</a>
                </div>
            </div>

            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
             <div id="subscribe">
                <form id="newsletter-signup" action="subscribe.php?action=signup" method="post">
                    <input type="email" name="signup-email" id="signup-email" class="news-mail" required placeholder="Enter email to subscribe"/>
                    <input type="submit" id="signup-button" value="SUBSCRIBE" class="news-button"/>
                    <div id='subscribe_msg'><span id="signup-response"></span></div>
                </form>
            </div>

            <div id="copyright">
                2017 &copy; OX SoftwareS. All rights reserved.
            </div>
        </footer>
    </body>
</html>
