<?php
session_start();
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
include 'database.php';
$obj = new User();
$wronguser = "";
if (isset($_POST['login'])) {
    $name = $_POST['name'];
    $pass = md5($_POST['pwd']);
    $obj->checkuser($name, $pass);
    $wronguser = "Invalid Username/password";
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Issue Tracker</title>
        <link rel="stylesheet" href="css/css_index.css">   
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/registrationvalidation.js"></script>
        <script src="js/subscribe_function.js"></script>         
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="index.php"><img src="images/oxlogo.png"></a> 
            </div>
            <div class="top-left">
                <a href="registration.php">Register</a> 
            </div>
        </header>
        <section>
            <div class="reg1">
                <h1>welcome to login page</h1>
                <form method="POST" action="index.php">
                    <div class="field">
                        <div class="namefield">
                            <label>Username</label>
                        </div>
                        <div class="textfield">
                            <input type="text" name="name" id="name">
                            <span id="nameerror"> </span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Password</label>
                        </div>
                        <div class="textfield">
                            <input type="password" name="pwd" id="pwd">
                            <span id="pwderror">

                            </span>

                        </div>
                    </div>
                    <button id ='login' name='login' vlaue='login'>login</button><br>
                    <a class="newuser" href="registration.php"> Not a member Click here </a><br>
                    <?php
                    if (isset($wronguser))
                        echo $wronguser;
                    ?>
                </form>
            </div>
        </section>
        <footer>
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <div id="socialmedia">  
                    <a class='social' title="Find us on Facebook" href="https://www.facebook.com/oxsoftwares" target="_blank">Facebook</a>
                </div>
                <div id="socialmedia"> 
                    <a class='social' title="Follow us on Twitter" href="https://twitter.com/oxsoftwares" target="_blank">Twitter</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Connect with us on Google+"  href="https://plus.google.com/+oxsoftwares" target="_blank">Google+</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Find us on LinkedIn" href="https://www.linkedin.com/company/oxsoftwares" tooltip=''target="_blank">LinkedIn</a>
                </div>
            </div>
            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form id="newsletter-signup" action="subscribe.php?action=signup" method="post">
                    <input type="email" name="signup-email" id="signup-email" class="news-mail" required placeholder="Enter email to subscribe"/>
                    <input type="submit" id="signup-button" value="SUBSCRIBE" class="news-button"/>
                    <div id='subscribe_msg'><span id="signup-response"></span></div>
                </form>
            </div>

            <div id="copyright">
                2017 &copy; OX SoftwareS. All rights reserved.
            </div>
        </footer>
    </body>
</html>
