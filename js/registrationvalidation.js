$(document).ready(function () {
    $('#submit').click(function () {

        var name_regex = /^[a-zA-Z]+$/;
        var name = $("#name").val();
        {
            if (name == "")

            {
                $("#nameerror").css("display", "inline");
                $('#nameerror').html("*This field is required");
                $("input").focus(function () {

                    $("#nameerror").css("display", "none");

                });
                return false;

            }
            if (!name.match(name_regex))
            {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html("Alphabets only");

                return false;

            }
            if ((name.length <= 3) || (name.length > 15))
            {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html("Name must have 3 to 15 characters");
                return false;
            }
        }
        var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        var email = $("#email").val();
        {

            if (email == "")

            {
                $("#emailerror").css("display", "inline");
                $('#emailerror').html("*This field is required");
                $("input").focus(function () {

                    $("#emailerror").css("display", "none");

                });
                return false;

            }
            if (!email.match(email_regex))
            {
                $("#emailerror").css("display", "inline");
                $("#emailerror").html("Enter vaild email id (Ex:john@gmail.com)");

                return false;

            }
        }
        var phone_regex = /^[0-9]+$/;
        var mobile = $("#mobile").val();
        {
            if (mobile == "")
            {
                $("#mobileerror").css("display", "inline");
                $('#mobileerror').html("*This field is required");
                $("input").focus(function () {

                    $("#mobileerror").css("display", "none");

                });
                return false;
            }
            if (!mobile.match(phone_regex))
            {
                $("#mobileerror").css("display", "inline");
                $("#mobileerror").html("Enter vaild mobile number (Ex:8667776387)");
                return false;

            }
            if ((mobile.length != 10))
            {
                $("#mobileerror").css("display", "inline");
                $("#mobileerror").html("Invalid Mobile number");
                return false;
            }
        }

        var pwd = $("#pwd").val();
        {
            if (pwd == "")

            {
                $("#pwderror").css("display", "inline");
                $('#pwderror').html("*This field is required");
                $("input").focus(function () {

                    $("#pwderror").css("display", "none");

                });
                return false;

            }
        }
        var conpwd = $("#conpwd").val();
        {
            if (conpwd == "")

            {
                $("#conpwderror").css("display", "inline");
                $('#conpwderror').html("*This field is required");
                $("input").focus(function () {

                    $("#conpwderror").css("display", "none");

                });
                return false;

            }
            if (pwd != conpwd)
            {
                $("#conpwderror").css("display", "inline");
                $('#conpwderror').html("*Password didn't match");
                return false;
            }
        }

        var role = $("#role").val();
        {
            if (role == "")
            {
                $("#roleerror").css("display", "inline");
                $('#roleerror').html("*This field is required");
                $("select").focus(function () {

                    $("#roleerror").css("display", "none");

                });

                return false;
            }
        }
    });
    $('#login').click(function () {

        var name_regex = /^[a-zA-Z]+$/;
        var name = $("#name").val();
        {
            if (name == "")

            {
                $("#nameerror").css("display", "inline");
                $('#nameerror').html("*This field is required");
                $("input").focus(function () {

                    $("#nameerror").css("display", "none");

                });
                return false;

            }
            if (!name.match(name_regex))
            {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html("Alphabets only");

                return false;

            }
            if ((name.length <= 3) || (name.length > 15))
            {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html("Name must have 3 to 15 characters");
                return false;
            }
        }

        var pwd = $("#pwd").val();
        {
            if (pwd == "")

            {
                $("#pwderror").css("display", "inline");
                $('#pwderror').html("*This field is required");
                $("input").focus(function () {

                    $("#pwderror").css("display", "none");

                });
                return false;

            }
        }
    });
});
