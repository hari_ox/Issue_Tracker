$(document).ready(function () {
    $('#submit').click(function () {


        var title = $("#title").val();
        {
            if (title == "")

            {
                $("#titleerror").css("display", "inline");
                $('#titleerror').html("*This field is required");
                $("input").focus(function () {

                    $("#titleerror").css("display", "none");

                });
                return false;

            }
            if((title.length <=5 ) || (title.length>=25)) 
            {
                 $("#titleerror").css("display", "inline");
                $('#titleerror').html("*Title length min:5 to max:15");
                $("input").focus(function () {

                    $("#titleerror").css("display", "none");

                });
                return false;

            }
        }
        var des = $("#des").val();
        {
            if (des == "")
            {
                $("#deserror").css("display", "inline");
                $('#deserror').html("*This field is required");
                $("textarea").focus(function () {

                    $("#deserror").css("display", "none");

                });
                return false;
            }
            if((des.length <=10 ) || (des.length>=100)) 
            {
                 $("#deserror").css("display", "inline");
                $('#deserror').html("*Description length min:10 to max:50");
                $("textarea").focus(function () {

                    $("#textarea").css("display", "none");

                });
                return false;

            }
        }


        var priority = $("#priority").val();
        {
            if (priority == "")
            {
                $("#priorityerror").css("display", "inline");
                $('#priorityerror').html("*This field is required");
                $("select").focus(function () {

                    $("#priorityerror").css("display", "none");

                });

                return false;
            }
        }
        var ext = $('#pic').val().split('.').pop().toLowerCase();
        if (ext == "")
        {
            $("#picerror").css("display", "inline");
            $('#picerror').html("*This field is required");
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#picerror").css("display", "inline");
            $('#picerror').html("*Format is wrong");
            return false;
        }
        
       
    });
});
