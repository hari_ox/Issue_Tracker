<?php
session_start();
$dbid=$_SESSION['userid'];
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
include 'database.php';
$obj = new User();
$success_message = '';
$error_message = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['submit'])) {
        $title = $_POST['title'];
        if (empty($title)) {
            $error_message = '<h3>Enter Issue Title</h3>';
            header('location:client_user_area.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        $des = $_POST['des'];
        if (empty($des)) {
            $error_message = '<h3>Enter Issue Description</h3>';
            header('location:client_user_area.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        $date = date('Y-m-d h-i-s');
        $udate = date('Y-m-d h-i-s');
        $priority = $_POST['priority'];
        if (empty($priority)) {
            $error_message = '<h3>Select the priority</h3>';
            header('location:client_user_area.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        if (isset($_FILES["pic"])) {
            $allowed = array("jpg" => "upload/jpg", "jpeg" => "upload/jpeg", "gif" => "upload/gif", "png" => "upload/png");
            $pict = $_FILES["pic"]["name"];
            $filetype = $_FILES["pic"]["type"];
            $filesize = $_FILES["pic"]["size"];
// Verify file extension
            $ext = pathinfo($pict, PATHINFO_EXTENSION);
            if (!array_key_exists($ext, $allowed)) {
                $error_message = '<h3>Error:Please select valid format</h3>';
                header('location:client_user_area.php');
                $_SESSION['error_msg'] = $error_message;
                return false;
            } else {
                move_uploaded_file($_FILES["pic"]["tmp_name"], "upload/" . $_FILES["pic"]["name"]);
            }

        }
        $status = "1";
        $assign = '0';
    }
    if ($obj->client_insert_issue($dbid, $title, $des, $date, $udate, $priority, $pict, $status,$assign)) {
        $success_message = '<h2 style="align:center">Issue Created!!</h2>';
        header('location:client_user_area.php');
        $_SESSION['success_msg'] = $success_message;
    }
}
?>


