<?php
session_start();
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
?>
<html>
    <head>
        <meta charset="UTF-8">

        <title>Registration page</title>
        <link rel="stylesheet" href="css/css_registration.css">  
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/registrationvalidation.js"></script>
        <script src="js/subscribe_function.js"></script>    
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="index.php"><img src="images/oxlogo.png"></a> 
            </div>
            <div class="top-left">
                <a href="index.php">Login</a> 
            </div>
        </header>
        <div class="dispmsg">
            <?php
            if (isset($_SESSION['success_msg'])) {
                echo $_SESSION['success_msg'];
            }
            if (isset($_SESSION['error_msg'])) {
                echo $_SESSION['error_msg'];
            }
            session_destroy();
            ?>
        </div>

        <section>


            <div class="reg1">

                <h1>Registration Form</h1>
                <form method="POST" action="registration_process.php">
                    <div class="field">
                        <div class="namefield">
                            <label>Name</label>
                        </div>
                        <div class="textfield">
                            <input type="text" name="name" id="name">
                            <span id="nameerror"></span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Email</label>
                        </div>
                        <div class="textfield">
                            <input type="text" name="email" id="email" value="">
                            <span id="emailerror"> </span>
                        </div>
                    </div>     
                    <div class="field">
                        <div class="namefield">
                            <label>Mobile</label>
                        </div>
                        <div class="textfield">
                            <input type="text" name="mobile" id="mobile" value="">
                            <span id="mobileerror">

                            </span>
                        </div>
                    </div>  
                    <div class="field">
                        <div class="namefield">
                            <label>Password</label>
                        </div>
                        <div class="textfield">
                            <input type="password" name="pwd" id="pwd" value="">
                            <span id="pwderror">

                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Conform password</label>
                        </div>
                        <div class="textfield">
                            <input type="password" name="conpwd" id="conpwd" value="">
                            <span id="conpwderror"> 
                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Role</label>
                        </div>
                        <div class="textfield">
                            <select name="role" id="role">
                                <option value="">Select your role</option>
                                <option value="1">Client</option>
                                <option value="2">Admin</option>
                                <option value="3">Super Admin</option>
                            </select>
                            <span id="roleerror"> </span>
                        </div>
                    </div>

                    <button id="submit" name="submit" value='submit'>SUBMIT</button><br>
                    <a class='existinguser' href="index.php">Already a member Login</a>

                </form>
            </div>

        </section>
        <footer>
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <div id="socialmedia">  
                <a class='social' title="Find us on Facebook" href="https://www.facebook.com/oxsoftwares" target="_blank">Facebook</a>
            </div>
            <div id="socialmedia">  
                <a class='social' title="Follow us on Twitter" href="https://twitter.com/oxsoftwares" target="_blank">Twitter</a>
            </div>
            <div id="socialmedia">  
                <a class='social' title="Connect with us on Google+"  href="https://plus.google.com/+oxsoftwares" target="_blank">Google+</a>
            </div>
            <div id="socialmedia">  
                <a class='social' title="Find us on LinkedIn" href="https://www.linkedin.com/company/oxsoftwares" tooltip=''target="_blank">LinkedIn</a>
            </div>
            </div>

            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form id="newsletter-signup" action="subscribe.php?action=signup" method="post">
                    <input type="email" name="signup-email" id="signup-email" class="news-mail" required placeholder="Enter email to subscribe"/>
                    <input type="submit" id="signup-button" value="SUBSCRIBE" class="news-button"/>
                    <div id='subscribe_msg'><span id="signup-response"></span></div>
                </form>
            </div>

            <div id="copyright">
                2017 &copy; OX SoftwareS. All rights reserved.
            </div>
        </footer>
    </body>
</html>
