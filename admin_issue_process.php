<?php

session_start();
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
include 'database.php';
$data = new User();
$closemsg = "";
if (isset($_POST['close'])) {
    $issueid = $_POST['id'];
    $update = date('Y-m-d h-i-s');
    $status = 'Closed';

    $data->admin_close_issue($issueid, $update, $status);
    $closemsg = '<h2 style="align:center">Issue Completed!!</h2>';
    header('location:admin_area.php');
    $_SESSION['closemsg'] = $closemsg;
}

$assignmsg = "";
if (isset($_POST['assign'])) {
    $issueid = $_POST['id'];
    $aname = $_POST['adminname'];
    $update = date('Y-m-d h-i-s');
    $status = 'Inprogress';
    $data->assign_issue($issueid, $aname, $update, $status);
   $assignmsg = '<h2 style="align:center">Issue Re-assigned!!</h2>';
    header('location:admin_area.php');
    $_SESSION['assignmsg'] = $assignmsg;
    $_SESSION['admin_name']=$aname;
    $_SESSION['issue_id']=$issueid;
  
}
?>
