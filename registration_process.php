<?php

session_start();
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
include 'database.php';
$obj = new User();
$success_message = '';
$error_message = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['submit'])) {
        $name = $_POST['name'];
        $string_exp = "/^[A-Za-z .'-]+$/";
        if (empty($name)) {
            $error_message = '<h3>Enter your name</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        } else {
            if (!preg_match($string_exp, $name)) {
                $error_message = '<h3>Enter vaild user name</h3>';
                header('location:registration.php');
                $_SESSION['error_msg'] = $error_message;
                return false;
            }
        }
        $email = $_POST['email'];
        $email_exp = '/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/';
        if (empty($email)) {
            $error_message = '<h3>Enter your email id</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        } else {
            if (!preg_match($email_exp, $email)) {
                $error_message = '<h3>Enter vaild email id</h3>';
                header('location:registration.php');
                $_SESSION['error_msg'] = $error_message;
                return false;
            }
        }
        $mobile = $_POST['mobile'];
        $mobile_exp = '/^[0-9]+$/';
        if (empty($mobile)) {
            $error_message = '<h3>Enter your mobile number</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        if (!empty($mobile)) {

            if (!preg_match($mobile_exp, $mobile)) {
                $error_message = '<h3>Enter valid mobile number</h3>';
                header('location:registration.php');
                $_SESSION['error_msg'] = $error_message;
                return false;
            }
        }
        $password = md5($_POST['pwd']);
        if (empty($password)) {
            $error_message = '<h3>Enter your password</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        $conpassword = md5($_POST['conpwd']);
        if (empty($conpassword)) {
            $error_message = '<h3>Enter conform password</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        if ($password != $conpassword) {
            $error_message = '<h3>Password didnt match</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
        $role = $_POST['role'];
        if (empty($role)) {
            $error_message = '<h3>Select your role</h3>';
            header('location:registration.php');
            $_SESSION['error_msg'] = $error_message;
            return false;
        }
else {

    $obj->registration($name, $email, $mobile, $password, $role);
    $success_message = '<h3>Registration Done</h3> <h4>Go to login page</h4>';
    header('location:registration.php');
    $_SESSION['success_msg'] = $success_message;
}
    }
}
?>

