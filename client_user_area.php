<?php
session_start();
include 'database.php';
$name = $_SESSION['username'];
$dbid = $_SESSION['userid'];
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $name; ?></title>
        <link rel="stylesheet" href="css/css_client_area.css">  
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/issue_jqvalidation.js"></script>
        <script src="js/subscribe_function.js"></script>    
    </head>
    <body>
        <header>
            <div class="logo">
                <a href="index.php"><img src="images/oxlogo.png"></a> 
            </div>

            <div class="top-left">
                <a href="logout.php">Logout</a> 
            </div>
            <div class="top-left">
                <?php
                if (!$name) {
                    header('location:index.php');
                } else {
                    echo "<h2>Welcome," . $name . "!</h2>";
                }
                ?>
            </div>
        </header>
        <section>
            <div class="reg1">
                <h1>Issue Form</h1>
                <form method="POST" action="issue_process.php" enctype="multipart/form-data">
                    <div class="field">
                        <div class="namefield">
                            <label>Title</label>
                        </div>
                        <div class="textfield">
                            <input type="text" name="title" id="title">
                            <span id="titleerror"></span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Description</label>
                        </div>
                        <div class="textfield">
                            <textarea name="des" id="des" value=""></textarea>
                            <span id="deserror"></span>
                        </div>
                    </div>     
                    <div class="field">
                        <div class="namefield">
                            <label>Priority</label>
                        </div>
                        <div class="textfield">
                            <select name="priority" id="priority">
                                <option value="">Select Priority</option>
                                <option value="1">low</option>
                                <option value="2">medium</option>
                                <option value="3">high</option>
                            </select>
                            <span id="priorityerror"></span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="namefield">
                            <label>Screen shot(only JPEG/PNG/JPG)</label>
                        </div>
                        <div class="textfield">
                            <input id="pic" type="file" name="pic" id= "pic" value=""> 
                            <span id="picerror"></span>
                        </div>
                    </div>
                    <button id="submit" name="submit" value='submit'>SUBMIT</button>
                </form>
            </div>
            <?php
            if (isset($_SESSION['success_msg'])) {
                echo $_SESSION['success_msg'];
                unset($_SESSION['success_msg']);
            }
            if (isset($_SESSION['error_msg'])) {
                echo $_SESSION['error_msg'];
                unset($_SESSION['success_msg']);
            }
            ?>
            <div id='issue_history'>
                <h2>Issue History</h2>


                <div class="issue_name"><h3>ID</h3></div>
                <div class="issue_name"><h3>Title</h3></div>
                <div class="issue_name"><h3>Created at</h3></div>
                <div class="issue_name"><h3>Updated at</h3></div>
                <div class="issue_name"><h3>Priority</h3></div>
                <div class="issue_name"><h3>Status</h3></div>
                <div class="issue_name"><h3>Action</h3></div>

                <?php
                $data = new User();
                $result = $data->client_issue($dbid);
                foreach ($result as $get) {
                    $issueid=$get['Issue_id'];
                    ?>

                        <div class="issue_text"><?php echo $get['Issue_id']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_title']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_created_at']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_updated_at']; ?></div>
                        <div class="issue_text"><?php
                            $priority = $get['Issue_priority'];
                            if ($priority == 1) {
                                echo 'Low';
                            }
                            if ($priority == 2) {
                                echo "Medium";
                            }
                            if ($priority == 3) {
                                echo "High";
                            }
                            ?></div>
                        <div class="issue_text"><?php
                            $status = $get['Issue_status'];
                            if ($status == 1) {
                                echo 'Open';
                            }
                            if ($status == 2) {
                                echo "Inprogress";
                            }
                            if ($status == 3) {
                                echo "Closed";
                            }
                            ?></div>
                        <div class="issue_text"><?php echo "<a href='issue_close_process.php?rowid=$issueid'>Close</a>" ?></div>

                <?php } ?>
                
                <?php
                    $result = $data->client_view_issue($dbid);
                    foreach ($result as $get) {
                    ?>
                        <div class="issue_text"><?php echo $get['Issue_id']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_title']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_created_at']; ?></div>
                        <div class="issue_text"><?php echo $get['Issue_updated_at']; ?></div>
                        <div class="issue_text"><?php
                            $priority = $get['Issue_priority'];
                            if ($priority == 1) {
                                echo 'Low';
                            }
                            if ($priority == 2) {
                                echo "Medium";
                            }
                            if ($priority == 3) {
                                echo "High";
                            }
                            ?></div>
                        <div class="issue_text"><?php
                            $status = $get['Issue_status'];
                            if ($status == 1) {
                                echo 'Open';
                            }
                            if ($status == 2) {
                                echo "Inprogress";
                            }
                            if ($status == 3) {
                                echo "Closed";
                            }
                            ?></div>
                        <div class="issue_text"></div>
                            
                            <?php }?>
                </div>



                <?php
                if (isset($_SESSION['updatemsg'])) {
                    echo $_SESSION['updatemsg'];
                    unset($_SESSION['updatemsg']);
                }
                ?>
        </section>
        <footer>
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <div id="socialmedia">
                    <a class='social' title="Find us on Facebook" href="https://www.facebook.com/oxsoftwares" target="_blank">Facebook</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Follow us on Twitter" href="https://twitter.com/oxsoftwares" target="_blank">Twitter</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Connect with us on Google+"  href="https://plus.google.com/+oxsoftwares" target="_blank">Google+</a>
                </div>
                <div id="socialmedia">  
                    <a class='social' title="Find us on LinkedIn" href="https://www.linkedin.com/company/oxsoftwares" tooltip=''target="_blank">LinkedIn</a>
                </div>
            </div>

            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
             <div id="subscribe">
                <form id="newsletter-signup" action="subscribe.php?action=signup" method="post">
                    <input type="email" name="signup-email" id="signup-email" class="news-mail" required placeholder="Enter email to subscribe"/>
                    <input type="submit" id="signup-button" value="SUBSCRIBE" class="news-button"/>
                    <div id='subscribe_msg'><span id="signup-response"></span></div>
                </form>
            </div>

            <div id="copyright">
                2017 &copy; OX SoftwareS. All rights reserved.
            </div>
        </footer>
    </body>
</html>
