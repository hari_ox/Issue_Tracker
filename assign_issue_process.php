<?php

session_start();
if (isset($_SESSION['username'])) {
    header('location:client_user_area.php');
}
if (isset($_SESSION['superadminname'])) {
    header('location:superadmin_area.php');
}
if (isset($_SESSION['adminname'])) {
    header('location:admin_area.php');
}
include 'database.php';
$data = new User();
$assignmsg = "";

$rowid = $_GET['rowid'];
echo $rowid;


$admin_id=$_GET['assignee'];
echo $admin_id;

$update = date('Y-m-d h-i-s');
$status = '2';
$data->assign_issue($rowid, $admin_id, $update, $status);
$assignmsg = '<h2 style="align:center">Issue Assigned!!</h2>';
header('location:admin_area.php');
$_SESSION['assignmsg'] = $assignmsg;
?>