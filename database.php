<?php

class User {

    private $db_host = 'localhost';
    private $db_user = 'root';
    private $db_pass = 'admin@123';
    private $db_name = 'project_issue_db';
    private $con;

    public function connect() {
        $this->con = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        if (mysqli_connect_errno()) {
            echo "Connection failed:" . mysqli_connect_error();
            exit();
        }
        return true;
    }

    public function registration($name, $email, $mobile, $password, $role) {
        $this->connect();
        $sql = "INSERT INTO project_user(User_name,User_email,User_mobile,User_password,User_role) 
        VALUES('$name', '$email','$mobile','$password','$role')";
        if (mysqli_query($this->con, $sql)) {
            return true;
        }
    }

    public function checkuser($name, $pass) {
        $var1 = 1;
        $var2 = 2;
        $var3 = 3;

        $this->connect();
        $sql = "SELECT User_id, User_name,User_password,User_role FROM project_user WHERE User_name='$name' AND User_password='$pass'";
        $result = mysqli_query($this->con, $sql) or die('Error');
        if (mysqli_num_rows($result) == 1) {
            while ($rows = mysqli_fetch_assoc($result)) {
                $dbid = $rows['User_id'];
                $dbrole = $rows['User_role'];
                if ($var1 == $dbrole) {
                    session_start();
                    header('location:client_user_area.php');
                    $_SESSION['username'] = $name;
                    $_SESSION['userid'] = $dbid;
                } else if ($var2 == $dbrole) {

                    session_start();
                    header('location:admin_area.php');
                    $_SESSION['adminname'] = $name;
                    $_SESSION['adminid'] = $dbid;
                } else if ($var3 == $dbrole) {

                    session_start();
                    header('location:superadmin_area.php');
                    $_SESSION['superadminname'] = $name;
                    $_SESSION['superadminid'] = $dbid;
                }
            }
        } else {
            return false;
        }
    }

    public function client_insert_issue($dbid, $title, $des, $date, $udate, $priority, $pict, $status, $assign) {

        $this->connect();
        $sql = "INSERT INTO project_issue(User_id,Issue_title,Issue_description,Issue_created_at,Issue_updated_at,Issue_priority,Issue_image,Issue_status,Issue_assigned_to) 
        VALUES('$dbid','$title','$des','$date','$udate','$priority','$pict','$status','$assign')";
        $result = mysqli_query($this->con, $sql) or die('Error');
        if ($result) {
            return true;
        }
    }

    public function client_issue($dbid) {
        $this->connect();
        $array = array();
        $sql = "select *from project_issue where User_id = '$dbid' AND Issue_status !='3' ORDER BY Issue_created_at DESC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function client_view_issue($dbid) {
        $this->connect();
        $array = array();
        $sql = "select * from project_issue where User_id = '$dbid' AND Issue_status ='3' ORDER BY Issue_created_at DESC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function client_close_issue($rowid, $update) {
        $this->connect();
        $sql = "UPDATE project_issue SET Issue_status='3',Issue_updated_at='$update' WHERE Issue_id='$rowid'";
        $result = mysqli_query($this->con, $sql);
        if ($result) {
            return true;
        }
    }

    public function superadmin_view_issue($dbid) {
        $this->connect();
        $array = array();
        $sql = "SELECT project_issue.Issue_id,project_issue.Issue_title,project_user.User_name,project_issue.Issue_created_at,project_issue.Issue_description,project_issue.Issue_priority
             FROM project_issue INNER JOIN project_user ON project_issue.User_id = project_user.User_id Where project_issue.Issue_status = '1' ORDER BY project_issue.Issue_created_at ASC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function select_admin_name() {
        $this->connect();
        $array = array();
        $sql = "SELECT User_id,User_name FROM project_user WHERE User_role='2'";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function assign_issue($rowid,$admin_id,$update, $status) {
        $this->connect();
        $sql = "UPDATE project_issue SET Issue_assigned_to='$admin_id',Issue_updated_at='$update',Issue_status='$status' WHERE Issue_id='$rowid'";
        $result = mysqli_query($this->con, $sql) or die('Error');
        if ($result) {
            return true;
        }
    }

    public function view_assigned_issue() {
        $this->connect();
        $array = array();
        $sql = "SELECT project_issue.Issue_id,project_issue.Issue_title,project_user.User_name,project_issue.Issue_created_at,project_issue.Issue_updated_at,project_issue.Issue_status,project_issue.Issue_priority,project_issue.Issue_description,project_issue.Issue_assigned_to
             FROM project_issue INNER JOIN project_user ON project_issue.User_id = project_user.User_id WHERE project_issue.Issue_status != '1' ORDER BY project_issue.Issue_created_at ASC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function assigned_to($admin_id) {
        $this->connect();
        $array = array();
        $sql = "SELECT User_name FROM project_user WHERE User_id = '$admin_id'";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function admin_view_issue($dbid) {
        $this->connect();
        $array = array();
        $sql = "SELECT project_issue.Issue_id,project_issue.Issue_title,project_issue.Issue_created_at,
            project_issue.Issue_updated_at,project_issue.Issue_priority,project_issue.Issue_status,project_issue.Issue_description,project_issue.Issue_assigned_to
             FROM project_issue INNER JOIN project_user ON 
             project_issue.User_id = project_user.User_id WHERE project_issue.Issue_assigned_to ='$dbid' AND  project_issue.Issue_status !='3' ORDER BY project_issue.Issue_priority DESC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

    public function admin_close_issue($issueid, $update, $status) {
        $this->connect();

        $sql = "UPDATE project_issue SET Issue_updated_at='$update',Issue_status='$status' WHERE Issue_id='$issueid'";
        $result = mysqli_query($this->con, $sql) or die('Error');
        if ($result) {
            return true;
        }
    }

    public function view_closed_issue($dbid) {
        $this->connect();
        $array = array();
        $sql = "SELECT project_issue.Issue_id,project_issue.Issue_title,project_issue.Issue_created_at,project_issue.Issue_updated_at,project_issue.Issue_status,project_issue.Issue_priority,project_issue.Issue_description
             FROM project_issue INNER JOIN project_user ON project_issue.User_id = project_user.User_id WHERE 
             project_issue.Issue_status ='3' AND project_issue.Issue_assigned_to = '$dbid' ORDER BY project_issue.Issue_priority DESC";
        $result = mysqli_query($this->con, $sql) or die('Error');
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = $row;
        }
        return $array;
    }

}

?>